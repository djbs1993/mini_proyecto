using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;
public class Trap : MonoBehaviour
{
    //private GameObject enemyHealth;
    //[SerializeField] GameObject sTrap;
    //EnemyHealth enemyh;
    //public bool active = false;
    //private StarterAssetsInputs starterAssetsInputs;
    private EnemyHealth enemyHealth;
    

    private void Start()
    {
        enemyHealth = GetComponent<EnemyHealth>();
        //starterAssetsInputs = GetComponent<StarterAssetsInputs>();
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "enemy" /*&& active==true*/)
        {
            StartCoroutine(WaitForSecondsToExplode());
            other.GetComponent<EnemyHealth>().enemyHealth = 0;

            //enemyHealth.enemyHealth -= 6;
            
            //Destroy(gameObject);
        }
    }

    IEnumerator WaitForSecondsToExplode()
    {
        yield return new WaitForSeconds(2);
        //active = true;

        Destroy(gameObject);


        //active = false;
    }
}
