using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuOptions : MonoBehaviour
{
    [SerializeField] GameObject start;
    [SerializeField] GameObject controls;
    [SerializeField] GameObject exit;
    [SerializeField] GameObject buttonPlacement;
    [SerializeField] GameObject backButton;

    // Start is called before the first frame update
    private void Awake()
    {
        buttonPlacement.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
       SceneManager.LoadScene("Playground");
       Cursor.visible = false; 
    }

    public void Controls()
    {
        backButton.gameObject.SetActive(true);
        buttonPlacement.gameObject.SetActive(true);
        start.gameObject.SetActive(false);
        controls.gameObject.SetActive(false);
        exit.gameObject.SetActive(false);
    }

    public void Back()
    {
        buttonPlacement.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);
        start.gameObject.SetActive(true);
        controls.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);
    }

    public void mainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
