using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MunitionCrate : MonoBehaviour
{
    private Munition ammo;
    private ThirdPersonShooterController trapAmount;
    private AudioSource mainAudioSource;
    [SerializeField] AudioClip GunMunition;

    private void Awake()
    {
        mainAudioSource = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioSource>();
        ammo = GetComponent<Munition>();
        trapAmount = GetComponent<ThirdPersonShooterController>();
    }

    private void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            mainAudioSource.PlayOneShot(GunMunition);
            other.GetComponent<ThirdPersonShooterController>().munition += 30;
            other.GetComponent<ThirdPersonShooterController>().secondMunition += 5;
            other.GetComponent<ThirdPersonShooterController>().trapAmount = 3;
            Destroy(gameObject);
        }
    }
}
