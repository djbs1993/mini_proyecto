using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCrate : MonoBehaviour
{

    private GameManager health;
    [SerializeField] GameManager GameManager;
    private AudioSource mainAudioSource;
    [SerializeField] AudioClip healthAquired;
    //Start is called before the first frame update

    private void Awake()
    {
        mainAudioSource = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioSource>();
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void Start()
    {
        //GameManager = GameObject.FindGameObjectWithTag("GameManager");
        health = /*GameManager.*/GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            mainAudioSource.PlayOneShot(healthAquired);
            GameManager.health = 4;
            Destroy(gameObject);
        }
    }
}
