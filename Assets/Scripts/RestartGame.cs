using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartGame : MonoBehaviour
{
    private GameManager health;
    private Munition bullets;
    
    [SerializeField] GameObject GameManager;
    [SerializeField] GameObject ThirdPersonShooterController;
    


    private void Start()
    {
        health = GameManager.GetComponent<GameManager>();
        bullets = ThirdPersonShooterController.GetComponent<Munition>();
    }
    public void RestartLevel()
    {
        PlayerPrefs.SetInt("munitionSaved", 36);
        Time.timeScale = 1;
        health.health = 4;
        bullets.munition = 36;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
