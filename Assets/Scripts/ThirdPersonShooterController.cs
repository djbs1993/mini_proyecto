using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class ThirdPersonShooterController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private float normalSensitivity;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    //[SerializeField] private Transform debugTransform;
    [SerializeField] private Transform pfBulletProjectile;
    [SerializeField] private Transform spawnBulletPosition;
    [SerializeField] private Transform SecondspawnBulletPosition;
    [SerializeField] private Transform ThirdspawnBulletPosition;
    [SerializeField] private Transform trapBomb;
    [SerializeField] GameObject StartMenu;
    [SerializeField] GameObject redDot;
    [SerializeField] GameObject GameManager;
    [SerializeField] GameObject pistol;
    [SerializeField] GameObject shotgun;
    [SerializeField] GameObject drawingPistol;
    [SerializeField] GameObject drawingShotgun;
    [SerializeField] AudioSource mainAudioSource;
    //[SerializeField] GameObject secondRestartButton;
    [SerializeField] TMP_Text TrapsNumber;
    [SerializeField] TMP_Text bullets;
    [SerializeField] TMP_Text topBullets;
    [SerializeField] TMP_Text SecondtopBullets;
    [SerializeField] TMP_Text secondBullets;
    public CameraShake cameraShake;
    public CameraShake secondCameraShake;

    private ThirdPersonController thirdPersonController;
    private StarterAssetsInputs starterAssetsInputs;
    private GameManager health;
    [SerializeField] AudioClip blasterClip;
    [SerializeField] AudioClip heavyBlasterClip;
    [SerializeField] AudioClip landMinePlacementClip;
    //private PauseMenu startMenu;
    private Trap trap;
    private Animator animator;
    private bool hasRecievedDamageRecently = false;
    private bool pause;
    private bool firstWeapon = false;
    private bool shooting = false;
    
    
    public int trapAmount = 3;
    public int munition = 36;
    public int secondMunition = 10;
    private int saveTrapAmount;
    private int saveMunitionAmount;
    private int saveSecondMunitionAmount;
    private Scene currentScene;
    private string sceneName;

    private void Awake()
    {
        
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        firstWeapon = true;
        
        cameraShake = cameraShake.GetComponent<CameraShake>();
        secondCameraShake = secondCameraShake.GetComponent<CameraShake>();
        health = GameManager.GetComponent<GameManager>();
        thirdPersonController = GetComponent<ThirdPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        
        //startMenu = GetComponent<PauseMenu>();
        trap = GetComponent<Trap>();
        animator = GetComponent<Animator>();
        StartMenu.gameObject.SetActive(false);
        drawingShotgun.gameObject.SetActive(false);
        munition = PlayerPrefs.GetInt("munitionSaved");
        secondMunition = PlayerPrefs.GetInt("secondMunitionSaved");
        trapAmount = PlayerPrefs.GetInt("trapAmountSaved");
        health.health = PlayerPrefs.GetInt("healthAmountSaved");

        
    }

    private void Start()
    {
        FirstLevel();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (munition > 99)
        {
            munition = 99;
        }

        if (secondMunition > 30)
        {
            secondMunition = 30;
        }

        if (secondMunition <= 0)
        {
            secondMunition = 0;
        }

        if (firstWeapon == true)
        {
            
            bullets.gameObject.SetActive(true);
            topBullets.gameObject.SetActive(true);
            secondBullets.gameObject.SetActive(false);
            SecondtopBullets.gameObject.SetActive(false);
        }

        if (firstWeapon == false)
        {
            
            bullets.gameObject.SetActive(false);
            topBullets.gameObject.SetActive(false);
            secondBullets.gameObject.SetActive(true);
            SecondtopBullets.gameObject.SetActive(true);
        }

        //if (playSound == true)
        //{
        //    fetchAudio.Play();
        //}

        //if (playSound == false)
        //{
        //    fetchAudio.Stop();
        //}
        
        TrapsNumber.text = trapAmount.ToString();
        bullets.text = munition.ToString();
        secondBullets.text = secondMunition.ToString();

        Vector3 mouseWorldPosition = Vector3.zero;

        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            //debugTransform.position = raycastHit.point;
            mouseWorldPosition = raycastHit.point;
        }

        if (starterAssetsInputs.aim && !pause)
        {
            shooting = true;
            aimVirtualCamera.gameObject.SetActive(true);
            thirdPersonController.SetSensitivity(aimSensitivity);
            thirdPersonController.SetRotationOnMove(false);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 1f, Time.deltaTime * 10f));

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;

            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);
        }

        else
        {
            shooting = false;
            aimVirtualCamera.gameObject.SetActive(false);
            thirdPersonController.SetSensitivity(normalSensitivity);
            thirdPersonController.SetRotationOnMove(true);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 0f, Time.deltaTime*10f));
        }

        if (starterAssetsInputs.shoot && !pause && munition > 0 && firstWeapon == true && shooting == true)
        {
            
            munition -= 1;
            Vector3 aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized;
            
            mainAudioSource.PlayOneShot(blasterClip);
            /*GameObject BalaInstanciada =*/ Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            
            starterAssetsInputs.shoot = false;

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;
        }
        
        if (starterAssetsInputs.shoot && !pause && secondMunition>0 && firstWeapon == false && shooting == true)
        {
            secondMunition -= 1;
            Vector3 aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized;
            
            mainAudioSource.PlayOneShot(heavyBlasterClip);
            /*GameObject BalaInstanciada =*/ Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            /*GameObject BalaInstanciada =*/ Instantiate(pfBulletProjectile, SecondspawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            /*GameObject BalaInstanciada =*/ Instantiate(pfBulletProjectile, ThirdspawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
            
            starterAssetsInputs.shoot = false;

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;
        }

        if (starterAssetsInputs.changeWeapon && firstWeapon == true)
        {
            drawingPistol.gameObject.SetActive(false);
            drawingShotgun.gameObject.SetActive(true);
            firstWeapon = false;
            pistol.gameObject.SetActive(false);
            shotgun.gameObject.SetActive(true);
            starterAssetsInputs.changeWeapon = false;
        }
        
        if (starterAssetsInputs.changeWeapon && firstWeapon == false)
        {
            drawingPistol.gameObject.SetActive(true);
            drawingShotgun.gameObject.SetActive(false);
            pistol.gameObject.SetActive(true);
            shotgun.gameObject.SetActive(false);
            firstWeapon = true;
            starterAssetsInputs.changeWeapon = false;
        }

        if (starterAssetsInputs.trap && !pause && trapAmount>0)
        {
            //trap.active = true;
            starterAssetsInputs.trap = false;
            mainAudioSource.PlayOneShot(landMinePlacementClip);
            trapAmount -= 1;
            Instantiate(trapBomb, transform.position,trapBomb.transform.rotation);
        }

        if (starterAssetsInputs.pause && !pause)
        {
            OnPauseState();
            //startMenu.StartMenu.gameObject.SetActive(true);
        }

        if (starterAssetsInputs.pause && pause)
        {
            OutOfPauseState();
            //startMenu.StartMenu.gameObject.SetActive(false);
        }

        

        PlayerPrefs.SetInt("munitionSaved", munition);
        PlayerPrefs.SetInt("secondMunitionSaved", secondMunition);
        PlayerPrefs.SetInt("trapAmountSaved", trapAmount);
        PlayerPrefs.SetInt("healthAmountSaved", health.health);

    }

    IEnumerator WaitForSecondsToInstantiate()
    {
        hasRecievedDamageRecently = true;
        cameraShake.ShakeCamera(1f, 0.2f);
        secondCameraShake.ShakeCamera(.5f, 0.2f);
        //StartCoroutine(cameraShake.Shake(.15f, .4f));
        health.health -= 1;
        yield return new WaitForSeconds(1f);

        hasRecievedDamageRecently = false; 
    }

    //Enemy Damage to Player
    private void OnTriggerEnter(Collider other)
    {
        if (!hasRecievedDamageRecently && other.gameObject.CompareTag("enemy"))
        {
            //CameraShake.Instance.ShakeCamera(5f, 0.5f);
            StartCoroutine(WaitForSecondsToInstantiate());
            //GameManager.health -= 1;
        }
    }

    public void OnPauseState()
    {
        pause = true;
        starterAssetsInputs.pause = false;
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        StartMenu.gameObject.SetActive(true);
        redDot.gameObject.SetActive(false);
        drawingPistol.gameObject.SetActive(false);
        drawingShotgun.gameObject.SetActive(false);
        
        TrapsNumber.gameObject.SetActive(false);
        
        
    }

    public void OutOfPauseState()
    {
        pause = false;
        starterAssetsInputs.pause = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        StartMenu.gameObject.SetActive(false);
        redDot.gameObject.SetActive(true);
        drawingPistol.gameObject.SetActive(true);
        drawingShotgun.gameObject.SetActive(true);

        TrapsNumber.gameObject.SetActive(true);
       
    }

    public void OutOfApp()
    {
        Application.Quit();
    }

    public void ReturnToHomeScreen()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void FirstLevel()
    {
        if (sceneName == "Playground")
        {
            trapAmount = 3;
            munition = 36;
            secondMunition = 10;
            health.health = 4;
            PlayerPrefs.DeleteKey("munitionSaved");
            PlayerPrefs.DeleteKey("secondMunitionSaved");
            PlayerPrefs.DeleteKey("trapAmountSaved");
            PlayerPrefs.DeleteKey("healthAmountSaved");
        }
    }
}