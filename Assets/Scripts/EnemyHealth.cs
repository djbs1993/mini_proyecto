using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private Transform vfxExplosion;
    private AudioSource mainAudioSource;
    [SerializeField] AudioClip explosion;
    private bool turnExplosionsOn;
    public int enemyHealth = 6;

    private void Start()
    {
        mainAudioSource = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioSource>();
    }
    void Update()
    {
        if (enemyHealth <= 0)
        {
            DeadEnemy();
        }
    }
    void DeadEnemy()
    {
        FindObjectOfType<EnemyRespawn>().enemies.Remove(this);
        if (!turnExplosionsOn)
        {
            mainAudioSource.PlayOneShot(explosion);
            Instantiate(vfxExplosion, transform.position, Quaternion.identity);
            turnExplosionsOn = true;
        }

        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            enemyHealth -= 1;
        }

        if (other.gameObject.tag == "Player")
        {
            enemyHealth = 0;
        }
    }
}
