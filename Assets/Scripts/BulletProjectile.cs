using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    [SerializeField] private Transform vfxHitGreen;
    [SerializeField] private Transform vfxHitRed;
    private Rigidbody bulletRigidbody;

    private void Awake()
    {
        bulletRigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        float speed = 35f;
        bulletRigidbody.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BulletTarget>() != null)
        {
            // Le dio al target
            Instantiate(vfxHitGreen, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        else
        {
            //Le dio a otra cosa que no es el target
            Instantiate(vfxHitRed, transform.position, Quaternion.identity);
            Destroy(gameObject);
            
        }
        
    }
}

