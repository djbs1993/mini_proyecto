using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyRespawn : MonoBehaviour
{
    //private int enemyAmount = 6;
    //[SerializeField] EnemyHealth enemyHealth;
    public List<EnemyHealth> enemies;
    [SerializeField] EnemyHealth enemyPrefab;
    [SerializeField] List<Transform> spawnPoints;
    //[SerializeField] GameObject munitionPrefab;
    //[SerializeField] GameObject healthCratePrefab;
    [SerializeField] int numberOfEnemiesPerWave = 10;
    [SerializeField] float timeToResetWave = 10f;
    [SerializeField] AudioSource EnemyDetectedAudioSource;
    [SerializeField] AudioClip enemyDetected;
    bool isSpawning = false;
    // Start is called before the first frame update

    void Start()
    {
        enemies = new List<EnemyHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        /*currentAmount = enemyHealth.enemyAmount;*/
        if (enemies.Count <= 0 && !isSpawning)
        {
            isSpawning = true;
            //Instantiate(munitionPrefab, FindObjectOfType<ThirdPersonShooterController>().transform.position + (Vector3.forward*2) + (Vector3.up * 1), munitionPrefab.transform.rotation);
            //Instantiate(healthCratePrefab, FindObjectOfType<ThirdPersonShooterController>().transform.position + (Vector3.forward*4) + (Vector3.up*1), healthCratePrefab.transform.rotation);
            StartCoroutine(WaitForSecondsToInstantiate());
        }   
    }

    IEnumerator WaitForSecondsToInstantiate()
    {
        isSpawning = true;
        yield return new WaitForSeconds(timeToResetWave);
        for (int i = 0; i < numberOfEnemiesPerWave; i++)
        {
            EnemyDetectedAudioSource.PlayOneShot(enemyDetected);
            var paco = Instantiate(enemyPrefab);
            enemies.Add(paco);
            paco.GetComponent<NavMeshAgent>().Warp(spawnPoints[i % spawnPoints.Count].position);
            Debug.DrawRay(spawnPoints[i % spawnPoints.Count].position, Vector3.up, Color.cyan, 5f);
        }
        isSpawning = false;
    }
}
