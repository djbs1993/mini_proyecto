using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    [SerializeField] GameObject SpawnPoint;
    public Vector3 spawnPoint;
    private GameManager health;
    [SerializeField] GameObject GameManager;
    private CharacterController player;
    [SerializeField] CharacterController characterController;
    // Start is called before the first frame update
    void Start()
    {
        health = GameManager.GetComponent<GameManager>();
        characterController = player.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    private void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.tag == "Player")
        {
            characterController.gameObject.SetActive(false);
            other.transform.position = new Vector3(3.32f, 0, 0);
            health.health -= 1;
            characterController.gameObject.SetActive(true);
        }
            
        
    }
}
