using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveBehaviour : MonoBehaviour
{
    float captureSpeed;
    [SerializeField] private GameObject exit;
    [SerializeField] private Transform flag;
    [SerializeField] private Transform top;
    [SerializeField] private GameObject progressBar;
    [SerializeField] AudioSource mainAudioSource;
    [SerializeField] AudioClip objectiveComplete;
    //public float fillbar;
    public float increaseBar;
    [SerializeField] private GameObject wayOut;
    public bool activeExit = false;
    
    
    //private Rigidbody flagRigidBody;
    float speed;
    public bool progress;

    private void Awake()
    {
        //flagRigidBody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        exit.SetActive(false);
        increaseBar = 0;
        progressBar.SetActive(false);
        /* GameObject.FindWithTag("ProgressBar");
        fillbar = GetComponent<FillBar>().current;*/

    }
    private void Update()
    {
        if(progress)
        {
            flag.transform.position = Vector3.MoveTowards(flag.transform.position, top.transform.position, speed * Time.deltaTime);
        }
        ObjectiveCompleted();
        
    }
    private void OnTriggerStay(Collider other)
    {
        //flagRigidBody.velocity = transform.up * speed;

        if (other.gameObject.tag == "Player")
        {
            progress = true;
        }

        if (progress == true)
        {
            speed = 0.05f;
            increaseBar = flag.transform.localPosition.y;
            //increaseBar += speed;

        }

        else
        {
            speed = 0;
        }

        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            progress = true;
            speed = 1f;
            increaseBar = flag.transform.position.y;
            progressBar.SetActive(true);
        }

        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            progressBar.SetActive(false);
            progress = false;
            //increaseBar = 0f;
        }
           
    }
    private void ObjectiveCompleted()
    {
        if (increaseBar >= 10 && activeExit==false)
        {
            activeExit = true;
            mainAudioSource.PlayOneShot(objectiveComplete);
            exit.SetActive(true);
            //Instantiate(wayOut, spawnPoint);
                 speed = 0;
            
        }
    }
}
