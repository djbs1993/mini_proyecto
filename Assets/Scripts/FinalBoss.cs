using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBoss : MonoBehaviour
{
    public int bossHealth = 60;
    [SerializeField] private Transform vfxExplosion;
    private GameManager health;
    [SerializeField] GameObject GameManager;
    [SerializeField] GameObject ExitDoor;
    [SerializeField] AudioSource mainAudioSource;
    [SerializeField] AudioSource enemyDetectedAudioSource;
    [SerializeField] AudioClip bossExplosion;
    [SerializeField] AudioClip enemyDetected;
    [SerializeField] AudioClip objectiveComplete;
    

    private void Start()
    {
        
        enemyDetectedAudioSource.PlayOneShot(enemyDetected);
        ExitDoor.gameObject.SetActive(false);
        health = GameManager.GetComponent<GameManager>();
    }
    void Update()
    {
        if (bossHealth <= 0)
        {
            ExitDoor.gameObject.SetActive(true);
            mainAudioSource.PlayOneShot(objectiveComplete);
            mainAudioSource.PlayOneShot(bossExplosion);
            Instantiate(vfxExplosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            bossHealth -= 1;
        }

        if (other.gameObject.tag == "Player")
        {
            health.health -= 1;
        }
    }

    
}
